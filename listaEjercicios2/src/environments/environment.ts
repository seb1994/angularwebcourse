// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCndtj2jSjNb9pb7x6lnJwPxtFTXdXUIbQ",
    authDomain: "lista-ejercicios-angular-3cd39.firebaseapp.com",
    databaseURL: "https://lista-ejercicios-angular-3cd39.firebaseio.com",
    projectId: "lista-ejercicios-angular-3cd39",
    storageBucket: "lista-ejercicios-angular-3cd39.appspot.com",
    messagingSenderId: "872499165518"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
