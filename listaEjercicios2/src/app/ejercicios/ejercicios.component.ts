import { Component, OnInit } from '@angular/core';
import {Ejercicio} from '../ejercicio';
import {EjercicioService}  from '../ejercicio.service';

@Component({
  selector: 'app-ejercicios',
  templateUrl: './ejercicios.component.html',
  styleUrls: ['./ejercicios.component.css']
})
export class EjerciciosComponent implements OnInit {

  ejercicios:Ejercicio[];
  ejercicioSel:Ejercicio;

  constructor(private ejercicioService:EjercicioService) {
    console.log("----- Componente Ejercicio creado (Ejercicio) ---");
   }

  ngOnInit(): void {
    console.log("----- Componente Ejercicio[ngOnInit] ---");
    this.getEjercicios();
  }

  /*
  Manera sincrona para consumir el servicio
  getEjercicios_old():void{
    this.ejercicios = this.ejercicioService.getEjercicios();
  }*/

  //Metodo asincrono para consumir el servicio a través de subscribe
  getEjercicios():void{
    this.ejercicioService.getEjercicios().subscribe(ejercicios=> this.ejercicios = ejercicios);
  }

  onSelectEjercicio(ejercicio:Ejercicio):void{
    console.log("Ejercicio Sel" + ejercicio.id);
    this.ejercicioSel = ejercicio;

  }

}
